# ENGLISH
## cname2onion
Bridge from clearnet to Tor hidden services.

The bridge takes in charge any protocol whatever its port, and also includes a demux for HTTP, HTTPS and SSH on 443.

Allows one to create CNAME record pointing to .onion addresse through the use of recursion during the DNS resolution process. It must be configured as the local resolver with a direct access to ip range reserved by tor. IPTable rules do the necessary redirections.


Use the following programs :
- Tor
- PowerDNS
- Apache2
- Main.go ( house cooked SSH/HTTP/HTTPS demux )


# FRENCH
## cname2onion
Pont depuis le clearnet vers les hidden services tor.

Le bridge prend en charges tous les protocoles sur leur ports d�di�s et contient un demultiplexeur HTTP, HTTPS, SSH sur 443.

Autorise la cr�ation d'enregistrement CNAME pointant vers un .onion en utilisant la r�cursion dans le processus de r�solution DNS, doit tourner comme r�solveur locale avec un acces direct � la plage d'ip r�serv� par tor. Des r�gles iptables font les redirections n�cessaires proprement.


Utilise les programmes suivants :
- Tor
- PowerDNS
- Apache2
- Main.go ( demultiplexeur SSH/HTTP/HTTPS )
